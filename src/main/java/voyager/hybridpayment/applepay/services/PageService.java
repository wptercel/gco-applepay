package voyager.hybridpayment.applepay.services;

import voyager.hybridpayment.applepay.models.ChargeDetail;
import voyager.hybridpayment.applepay.models.EState;
import voyager.hybridpayment.applepay.infra.Database;
import voyager.hybridpayment.applepay.models.PaymentPage;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

@Path("paymentpage")
public class PageService {
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getPaymentPage(@QueryParam("guid") String guid) {
        PaymentPage paymentPage = Database.getDatabase().get(UUID.fromString(guid));
        paymentPage.setCurrentState(EState.OPENED);
        return "<h1>" + paymentPage.getCurrentState() + "</h1>";
    }
}
