package voyager.hybridpayment.applepay.infra;

import voyager.hybridpayment.applepay.models.ChargeDetail;
import voyager.hybridpayment.applepay.models.EState;
import voyager.hybridpayment.applepay.models.PaymentPage;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static java.util.UUID.fromString;

public class Database {
    private static Map<UUID, PaymentPage> dataBase = new HashMap<>();

    static{
        // for test
        PaymentPage paymentPage = new PaymentPage();
        paymentPage.setCurrentState(EState.CREATED);
        dataBase.put(fromString("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"), paymentPage);
    }

    private Database(){}

    public static Map<UUID, PaymentPage> getDatabase(){
        if(dataBase == null){
            dataBase = new HashMap<UUID, PaymentPage>();
        }
        return dataBase;
    }

}
