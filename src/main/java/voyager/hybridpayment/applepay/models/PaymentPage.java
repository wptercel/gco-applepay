package voyager.hybridpayment.applepay.models;

public class PaymentPage {
    private State currentState;
    private ChargeDetail chargeDetail;

    public PaymentPage(){
        currentState = new State();
    }

    public EState getCurrentState() {
        return currentState.getCurrentState();
    }

    public void setCurrentState(EState newState) {
        switch (newState){
            case CREATED:
                currentState.create();
                break;
            case OPENED:
                currentState.open();
                break;
            case PROCESSING:
                currentState.process();
                break;
            case PAID:
                currentState.succeed();
                break;
            case FAILED:
                currentState.fail();
                break;
            case EXPIRED:
                currentState.expire();
                break;
            case UNKNOWN:
            default:
                break;
        }
    }

    public ChargeDetail getChargeDetail() {
        return chargeDetail;
    }

    public void setChargeDetail(ChargeDetail chargeDetail) {
        this.chargeDetail = chargeDetail;
    }
}
