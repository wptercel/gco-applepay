package voyager.hybridpayment.applepay.models;

public enum EState {
    CREATED,    // Page created
    OPENED,     // Page opened by customer
    PROCESSING, // Under ApplePay processing
    PAID,       // Payment succeeded
    FAILED,     // Payment failed, can be open again
    EXPIRED,    // After 25 min
    UNKNOWN;    // All other status
}
