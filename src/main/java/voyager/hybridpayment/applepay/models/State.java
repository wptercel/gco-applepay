package voyager.hybridpayment.applepay.models;

import java.util.Calendar;

public class State {
    final int TTL = 25; // minute

    private EState currentState;
    private Calendar createdAt;

    public State(){
        currentState = EState.UNKNOWN;
    }

    public EState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(EState currentState) {
        this.currentState = currentState;
    }

    public EState create(){
        EState newState = EState.CREATED;
        if(currentState == EState.UNKNOWN){
            newState = EState.CREATED;
        }
        else{
            throw new IllegalStateChangeException("Illegal state change: " + currentState + " -> " + newState);
        }
        currentState = newState;
        return currentState;
    }

    public EState open(){
        EState newState = EState.OPENED;
        boolean isExpired = tryExpire();
        if(isExpired) {
            throw new ExpiredStateException();
        }

        if(currentState == EState.CREATED){
            currentState = newState;
        }
        else{
            throw new IllegalStateChangeException("Illegal state change: " + currentState + " -> " + newState);
        }
        return currentState;
    }

    public EState process(){
        EState newState = EState.PROCESSING;
        boolean isExpired = tryExpire();
        if(isExpired) {
            throw new ExpiredStateException();
        }

        if(currentState == EState.OPENED){
            currentState = newState;
        }
        else{
            throw new IllegalStateChangeException("Illegal state change: " + currentState + " -> " + newState);
        }
        return currentState;
    }

    public EState succeed(){
        EState newState = EState.PAID;
        boolean isExpired = tryExpire();
        if(isExpired) {
            throw new ExpiredStateException();
        }

        if(currentState == EState.PROCESSING){
            currentState = newState;
        }
        else{
            throw new IllegalStateChangeException("Illegal state change: " + currentState + " -> " + newState);
        }
        return currentState;
    }

    public EState fail(){
        EState newState = EState.FAILED;
        boolean isExpired = tryExpire();
        if(isExpired) {
            throw new ExpiredStateException();
        }

        if(currentState == EState.PROCESSING){
            currentState = newState;
        }
        else{
            throw new IllegalStateChangeException("Illegal state change: " + currentState + " -> " + newState);
        }
        return currentState;
    }

    public EState expire(){
        EState newState = EState.EXPIRED;
        if(currentState == EState.UNKNOWN){
            throw new IllegalStateChangeException("Illegal state change: " + currentState + " -> " + newState);
        }
        currentState = newState;
        return currentState;
    }

    private boolean tryExpire(){
        boolean isExpired = false;
        if(createdAt != null){
            Calendar expirationTime = Calendar.getInstance();
            expirationTime.add(Calendar.MINUTE, TTL);
            isExpired = expirationTime.compareTo(createdAt) < 0;
        }
        if(isExpired){
            expire();
        }
        return isExpired;
    }
}
