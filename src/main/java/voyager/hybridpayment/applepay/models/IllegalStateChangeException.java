package voyager.hybridpayment.applepay.models;

public class IllegalStateChangeException extends RuntimeException {
    public IllegalStateChangeException(String desc){
        super(desc);
    }
}
